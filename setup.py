from setuptools import setup, find_packages

setup(
    name='webscan',
    version='0.0a1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'gevent>=1.1b3',
        'lxml',
        'requests',
        'html5lib',
        ],
    entry_points='''
        [console_scripts]
        webscan=webscan.cli:main
        ''',
    )

