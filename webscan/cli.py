import click
from webscan import core

import logging
import csv


@click.command()
@click.option('--debug/--no-debug', default=False)
@click.argument('url')
def main(debug, url):
    logger = logging.getLogger('webscan.core')

    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    results = core.begin_scan(url)

    #click.echo(results)
    stdout = click.get_text_stream('stdout')

    writer = csv.writer(stdout)
    writer.writerow(['URL', 'Status'])

    for result in results:
        writer.writerow([result.url, result.status])
