import gevent
from gevent.queue import Queue
from gevent import monkey, Greenlet

monkey.patch_all()

import requests
import logging
from collections import namedtuple, deque

import lxml.html
from lxml.html import html5parser

import urllib.parse

import networkx as nx

from matplotlib import pyplot as plt

logger = logging.getLogger('webscan.core')
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

Result = namedtuple('Result', ['status', 'url', 'headers'])


def begin_scan(url):
    mgr = ScanManager(url)
    results = mgr.start()

    output = []
    while True:
        try:
            output.append(results.get_nowait())
        except gevent.queue.Empty:
            break

    # edge_count = len(mgr.G.edges)
    max_edges = max([len(mgr.G.adj[n]) for n in mgr.G.nodes])
    heatmap = [(len(mgr.G.adj[n]) / max_edges) for n in mgr.G.nodes]

    # print(heatmap)

    # plt.subplot(111)
    fig = plt.figure(figsize=(20, 20))
    fig.add_subplot(111)
    nx.draw_kamada_kawai(mgr.G, with_labels=True, node_size=40, node_shape='.',
            edge_color='#cccccc',
            node_color=heatmap, vmin=0.0, vmax=1.0,
            cmap=plt.get_cmap('hot'))

    plt.savefig('/Users/matt/Desktop/graph.svg', bbox_inches="tight", dpi=100)

    return output


class ScanManager(Greenlet):
    def __init__(self, root_url):
        super(Greenlet, self).__init__()

        self.max_workers = 5

        self.root_url = root_url
        if not self.root_url.endswith('/'):
            self.root_url = '{}/'.format(self.root_url)

        self.workers = []

        self.scanned = []
        self.to_scan = deque()
        self.incoming = Queue()

        self.results = Queue()

        self.to_scan.append(self.root_url)

        self.session = requests.Session()

        self.G = nx.Graph()

    def start(self):

        while len(self.to_scan) > 0:

            spawn_count = min(self.max_workers, len(self.to_scan))

            for i in range(spawn_count):
                next_url = self.to_scan.popleft()

                if next_url not in self.scanned:
                    w = gevent.spawn(scan_worker, self, next_url)
                    self.workers.append(w)
                    self.scanned.append(next_url)

            gevent.wait(self.workers)

            while True:
                try:
                    url, referrer = self.incoming.get(block=False)
                    if url not in self.scanned:
                        self.to_scan.append(url)

                    r = referrer.replace(self.root_url, '')
                    u = url.replace(self.root_url, '')
                    self.G.add_edge(r, u)

                except gevent.queue.Empty:
                    break

        return self.results



def scan_worker(manager, target_url):
    global logger

    try:
        r = manager.session.head(target_url, timeout=5)
    except requests.RequestException as ex:
        logger.error('Request Failed: %s\n%s', r.url, str(ex))
        return

    if not r.ok or not r.headers['content-type'].startswith('text/html'):
        logger.debug('%s %s', r.url, r.status_code)
        res = Result(status=r.status_code, url=r.url, headers=r.headers)
        manager.results.put_nowait(res)
        return

    try:
        r = manager.session.get(target_url, timeout=5)
    except requests.RequestException as ex:
        logger.error('2nd Request Failed: %s\n%s', r.url, str(ex))
        return

    if not r.ok or not r.headers['content-type'].startswith('text/html'):
        #this would be weird if this happens...
        # or it's a wordpress site
        logger.debug('%s %s', r.url, r.status_code)
        res = Result(status=r.status_code, url=r.url, headers=r.headers)
        manager.results.put_nowait(res)
        return

    logger.debug('%s %s', r.url, r.status_code)

    doc = html5parser.document_fromstring(r.text.encode('utf-8'))
    doc = lxml.html.HtmlElement(doc)

    doc.make_links_absolute(manager.root_url)

    for element, attribute, link, pos in doc.iterlinks():
        if manager.root_url not in link:
            continue

        urlinfo = urllib.parse.urlparse(link)
        if urlinfo.scheme not in ('http', 'https',):
            continue

        link = urllib.parse.urldefrag(link).url
        manager.incoming.put_nowait((link, target_url))

    res = Result(status=r.status_code, url=r.url, headers=r.headers)
    manager.results.put_nowait(res)
